import { createSlice } from "@reduxjs/toolkit";
import { localUserServ } from "../service/locailStoage/locailAdmin";

const initialState = {
  userInfor: localUserServ.get(),
};

const useSlice = createSlice({
  name: "useSlice",
  initialState,
  reducers: {
    setDataUser: (state, action) => {
      state.userInfor = action.payload;
    },
  },
});

export const { setDataUser } = useSlice.actions;
export default useSlice.reducer;

// rxslice
