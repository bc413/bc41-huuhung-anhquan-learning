import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  isLoading: false,
};

const spinerSlice = createSlice({
  name: "spinerSlice",
  initialState,
  reducers: {
    setLoadingOn: (state, action) => {
      state.isLoading = true;
    },
    setLoadingOff: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const { setLoadingOn, setLoadingOff } = spinerSlice.actions;
export default spinerSlice.reducer;
