import { Button, Checkbox, Form, Input, message } from "antd";
import { getData } from "../../service/useAdminService";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { localUserServ } from "../../service/locailStoage/locailAdmin";
import { setDataUser } from "../../toolkit/useSlice";

const LoginPage = () => {
  let navLink = useNavigate();

  let { dataUse } = useSelector((state) => {
    return state.useSlice;
  });
  let dispatch = useDispatch();

  const onFinish = (values) => {
    console.log("Success:", values);
    getData
      .loginAdmin(values)
      .then((res) => {
        console.log(res.data);
        localUserServ.set(res.data);
        dispatch(setDataUser(res.data));
        message.success("Đăng nhập thành công!");
        navLink("/");
      })
      .catch((err) => {
        console.log(err);
        message.error("Tài khoản hoặc mật khẩu sai");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="flex justify-center items-center h-screen bg-orange-500">
      <div className=" p-20 rounded-xl bg-white">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button className="bg-lime-500" type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
export default LoginPage;
