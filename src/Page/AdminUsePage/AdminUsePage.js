import React, { useEffect, useState } from "react";
import { getData } from "../../service/useAdminService";
import { Button, Form, Input, Modal, Select, Table, message } from "antd";
import { columns } from "./utils";
import Spiner from "../../components/Spiner";
import { useDispatch } from "react-redux";
import { setLoadingOff, setLoadingOn } from "../../toolkit/SpinSlice";
import { Option } from "antd/es/mentions";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

export default function AdminUsePage() {
  const [listNguoiDung, setListNguoiDung] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [buttonUpdate, setButtonUpdate] = useState(false);
  const [valueEdit, setValueEdit] = useState("");
  const [form] = Form.useForm();
  const [componentDisabled, setComponentDisabled] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
    setButtonUpdate(false);
    setValueEdit("");
    setComponentDisabled(false);
  };
  const handleOk = () => {
    setIsModalOpen(false);
    setValueEdit("");
  };
  const handleCancel = () => {
    setIsModalOpen(false);
    setValueEdit("");
  };

  const handleEdit = (dataEdit) => {
    setIsModalOpen(true);
    setButtonUpdate(true);
    setComponentDisabled(true);
    setValueEdit(dataEdit);
  };

  const onFinish = (values) => {
    getData
      .userRegister(values)
      .then((res) => {
        message.success("Đăng ký thành công!");
        setIsModalOpen(false);
        setValueEdit("");
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng ký thất bại!");
      });
  };

  let dispatch = useDispatch();
  let handleDelete = (taiKhoan) => {
    getData
      .deleteUser(taiKhoan)
      .then((res) => {
        dispatch(setLoadingOff());
        console.log(res);
        message.success("Đã xóa thành công!");
        fetchUserList();
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
        message.error("Đã có lỗi xảy ra!");
      });
  };
  let fetchUserList = () => {
    getData
      .getUseData()
      .then((res) => {
        dispatch(setLoadingOff());
        let Arr = res.data.map((item, index) => {
          return {
            ...item,
            button: (
              <div className="flex">
                <Button
                  onClick={() => {
                    handleDelete(item.taiKhoan);
                  }}
                  className="mr-3 bg-[#FDE8E8] text-[#9B1C1C] border-none font-bold text-xs"
                >
                  Xóa
                </Button>
                <Button
                  onClick={() => {
                    handleEdit(item);
                  }}
                  className="bg-[#f7ede2] te text-[#f6bd60] border-none  font-bold text-xs"
                >
                  Sửa
                </Button>
              </div>
            ),
          };
        });
        setListNguoiDung(Arr);
      })
      .catch((err) => {
        console.log(err);
        dispatch(setLoadingOff());
      });
  };

  useEffect(() => {
    dispatch(setLoadingOn());
    fetchUserList();
  }, []);

  let handleOnchange = (e) => {
    let { value } = e.target;
    if (value) {
      getData
        .getSearchUser(value)
        .then((res) => {
          console.log(res);
          let listUser = res.data.map((item) => {
            return {
              ...item,
              button: (
                <div className="flex">
                  <Button
                    onClick={() => {
                      handleDelete(item.taiKhoan);
                    }}
                    className="mr-3 bg-[#FDE8E8] text-[#9B1C1C] border-none font-bold text-xs"
                  >
                    Xóa
                  </Button>
                  <Button
                    onClick={() => {
                      handleEdit(item);
                    }}
                    className="bg-[#f7ede2] te text-[#f6bd60] border-none  font-bold text-xs"
                  >
                    Sửa
                  </Button>
                </div>
              ),
            };
          });
          setListNguoiDung(listUser);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      fetchUserList();
    }
  };

  const handleEditUser = () => {
    const taiKhoan = form.getFieldValue("taiKhoan");
    const matKhau = form.getFieldValue("matKhau");
    const hoTen = form.getFieldValue("hoTen");
    const soDT = form.getFieldValue("soDT");
    const maLoaiNguoiDung = form.getFieldValue("maLoaiNguoiDung");
    const maNhom = form.getFieldValue("maNhom");
    const email = form.getFieldValue("email");

    let dataEditUser = {
      taiKhoan,
      matKhau,
      hoTen,
      soDT,
      maLoaiNguoiDung,
      maNhom,
      email,
    };
    getData
      .updateUser(dataEditUser)
      .then((res) => {
        message.success("Cập nhật thành công!");
        fetchUserList();
        setIsModalOpen(false);
        setValueEdit("");
        setTimeout(() => {
          window.location.reload();
        }, 700);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã có lỗi xảy ra!");
      });
  };

  return (
    <div>
      <Button className="bg-[#1677ff]" type="primary" onClick={showModal}>
        Thêm người dùng
      </Button>
      <Modal
        title={!buttonUpdate ? "Thêm Người Dùng" : "Chỉnh Sửa Người Dùng"}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[]}
      >
        <Form
          form={form}
          {...formItemLayout}
          name="register"
          onFinish={onFinish}
          initialValues={{
            residence: ["zhejiang", "hangzhou", "xihu"],
            prefix: "86",
          }}
          style={{
            maxWidth: 600,
          }}
          scrollToFirstError
        >
          <Form.Item
            initialValue={valueEdit ? valueEdit.taiKhoan : ""}
            name="taiKhoan"
            label="Tài Khoản"
            rules={[
              {
                type: "taiKhoan",
                message: "The input is not valid",
              },
              {
                required: true,
                message: "Hãy nhập tài khoản",
              },
            ]}
          >
            <Input disabled={componentDisabled} />
          </Form.Item>
          <Form.Item
            initialValue={valueEdit ? valueEdit.hoTen : ""}
            name="hoTen"
            label="Họ tên"
            rules={[
              {
                type: "hoTen",
                message: "The input is not valid",
              },
              {
                required: true,
                message: "Hãy nhập họ tên",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={valueEdit ? valueEdit.email : ""}
            name="email"
            label="E-mail"
            rules={[
              {
                type: "email",
                message: "Hãy nhập email",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={valueEdit ? valueEdit.matKhau : ""}
            name="matKhau"
            label="Password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            initialValue={valueEdit ? valueEdit.matKhau : ""}
            name="confirm"
            label="Confirm Password"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("matKhau") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(
                      "The two passwords that you entered do not match!"
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            initialValue={valueEdit ? valueEdit.maLoaiNguoiDung : ""}
            name="maLoaiNguoiDung"
            label="Loại người dùng"
            rules={[
              {
                required: true,
                message: "Hãy chọn loại người dùng",
              },
            ]}
          >
            <Select placeholder="select your gender">
              <Option value="GV">GV</Option>
              <Option value="HV">HV</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={"GP01"}
            name="maNhom"
            label="Mã Nhóm"
            rules={[
              {
                required: true,
                message: "Hãy chọn mã nhóm",
              },
            ]}
          >
            <Select
              disabled={componentDisabled}
              placeholder="select your gender"
            >
              <Option value="GP01">GP01</Option>
              <Option value="GP02">GP02</Option>
              <Option value="GP03">GP03</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={valueEdit ? valueEdit.soDt : ""}
            name="soDT"
            label="Số điện thoại"
            rules={[
              {
                required: true,
                message: "The input is not valid",
              },
              {
                required: true,
                message: "Hãy nhập số điện thoại",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <div className="flex justify-between">
              {!buttonUpdate ? (
                <Button
                  className="bg-green-600"
                  type="primary"
                  htmlType="submit"
                >
                  Đăng ký
                </Button>
              ) : (
                <Button
                  className="bg-green-600"
                  type="primary"
                  onClick={handleEditUser}
                >
                  Cập nhật
                </Button>
              )}

              <Button
                onClick={handleCancel}
                type="primary"
                className="bg-[#faedcd] text-[#d4a373]"
              >
                Huỷ bỏ
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Modal>
      <Input
        onChange={handleOnchange}
        className="py-3 my-5"
        placeholder="Tìm kiếm người dùng"
      />
      <Table columns={columns} dataSource={listNguoiDung} />
    </div>
  );
}
