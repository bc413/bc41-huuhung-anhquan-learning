import React, { useEffect, useState } from "react";
import { columns } from "./utils";
import {
  Button,
  Form,
  Input,
  Modal,
  Table,
  message,
  Upload,
  Select,
  DatePicker,
} from "antd";
import { getData } from "../../service/useAdminService";
import { useDispatch } from "react-redux";
import { setLoadingOff, setLoadingOn } from "../../toolkit/SpinSlice";
import { PlusOutlined } from "@ant-design/icons";
import { Option } from "antd/es/mentions";
import moment from "moment";
import { localUserServ } from "../../service/locailStoage/locailAdmin";

export default function AdminMoviePage() {
  const [listCourse, setListCourse] = useState([]);
  let dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  let handleDelete = (maKhoaHoc) => {
    getData
      .deleteCourse(maKhoaHoc)
      .then((res) => {
        dispatch(setLoadingOff());
        console.log(res);
        message.success("Đã xóa thành công!");
        fetchCourseList();
      })
      .catch((err) => {
        dispatch(setLoadingOff());
        console.log(err);
        message.error("Đã có lỗi xảy ra!");
      });
  };
  let fetchCourseList = () => {
    getData
      .getCourseData()
      .then((res) => {
        let ArrayListCourse = res.data.map((item) => {
          return {
            ...item,
            button: (
              <div className="flex">
                <Button
                  onClick={() => {
                    handleDelete(item.maKhoaHoc);
                  }}
                  className="mr-3 bg-[#FDE8E8] text-[#9B1C1C] border-none font-bold text-xs"
                >
                  Xóa
                </Button>
                <Button className="bg-[#f7ede2] te text-[#f6bd60] border-none  font-bold text-xs">
                  Sửa
                </Button>
              </div>
            ),
          };
        });
        dispatch(setLoadingOff());
        setListCourse(ArrayListCourse);
      })
      .catch((err) => {
        console.log(err);
        dispatch(setLoadingOff());
      });
  };

  const [danhMucKhoaHoc, setDanhMucKhoaHoc] = useState("");
  const [taiKhoanNguoiTao, setTaiKhoanNguoiTao] = useState("");

  useEffect(() => {
    fetchCourseList();
    dispatch(setLoadingOn());
    const taiKhoanTao = localUserServ.get();
    setTaiKhoanNguoiTao(taiKhoanTao.taiKhoan);
    getData
      .layDanhMucKhoaHoc()
      .then((res) => {
        setDanhMucKhoaHoc(res.data);
        setIsModalOpen(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const optionsDanhMuc = () => {
    if (danhMucKhoaHoc) {
      return danhMucKhoaHoc.map((item) => {
        return <Option value={`${item.maDanhMuc}`}>{item.tenDanhMuc}</Option>;
      });
    }
  };

  let handleOnchange = (e) => {
    let { value } = e.target;
    if (value) {
      getData
        .getSearchCourse(value)
        .then((res) => {
          console.log(res);
          let listCourse = res.data.map((item) => {
            return {
              ...item,
              button: (
                <div className="flex">
                  <Button
                    onClick={() => {
                      handleDelete(item.maKhoaHoc);
                    }}
                    className="mr-3 bg-[#FDE8E8] text-[#9B1C1C] border-none font-bold text-xs"
                  >
                    Xóa
                  </Button>
                  <Button className="bg-[#f7ede2] te text-[#f6bd60] border-none  font-bold text-xs">
                    Sửa
                  </Button>
                </div>
              ),
            };
          });
          setListCourse(listCourse);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      fetchCourseList();
    }
  };

  const [fileList, setFileList] = useState([]);
  const handleFileChange = ({ fileList }) => {
    setFileList(fileList);
  };

  const [dates, setDates] = useState("");
  const handleDatePickerChange = (date, dateString) => {
    const formattedDate = moment(dateString).format("DD/MM/YYYY");
    setDates(formattedDate);
  };
  const onFinish = (values) => {
    let nameImg = fileList.length > 0 ? fileList[0].originFileObj.name : "";
    const newValue = {
      ...values,
      luotXem: 0,
      danhGia: 0,
      hinhAnh: nameImg,
      ngayTao: dates,
    };
    console.log(newValue);
    getData
      .themKhoaHoc(newValue)
      .then((res) => {
        console.log(res);
        message.success("Thêm Khóa học thành công!");
        fetchCourseList();
        setIsModalOpen(false);
        form.resetFields();
        let dataImg = fileList.length > 0 ? fileList[0].originFileObj : "";
        let frm = new FormData();
        frm.append("file", dataImg);
        frm.append("tenKhoaHoc", "backend");
        getData
          .upLoadHinhAnh(frm)
          .then((res) => {
            console.log(res);
            message.success("Thêm hình ảnh thành công");
          })
          .catch((err) => {
            console.log(err);
            message.error("Thêm hình ảnh thất bại");
          });
      })
      .catch((err) => {
        console.log(err);
        message.error("Thêm Khóa học thất bại!");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <>
        <Button className="bg-[#1677ff]" type="primary" onClick={showModal}>
          Thêm khóa học
        </Button>
        <Modal
          title="Thêm khóa học"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={[]}
        >
          <Form
            form={form}
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Mã khóa học"
              name="maKhoaHoc"
              rules={[
                {
                  required: true,
                  message: "Hãy nhập mã khóa học!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Tên khóa học"
              name="tenKhoaHoc"
              rules={[
                {
                  required: true,
                  message: "Hãy nhập mã khóa học!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item label="Mô Tả" name="moTa">
              <TextArea rows={4} />
            </Form.Item>

            <Form.Item label="Tải lên hình ảnh" valuePropName="fileList">
              <Upload
                listType="picture-card"
                onChange={handleFileChange}
                fileList={fileList}
              >
                <div>
                  <PlusOutlined />
                  <div style={{ marginTop: 8 }}>Upload</div>
                </div>
              </Upload>
            </Form.Item>

            <Form.Item
              name="maDanhMucKhoaHoc"
              label="Danh mục khóa học"
              rules={[
                {
                  required: true,
                  message: "Hãy danh mục khóa học",
                },
              ]}
            >
              <Select placeholder="select your gender">
                {optionsDanhMuc()}
              </Select>
            </Form.Item>

            <Form.Item
              initialValue={"GP01"}
              name="maNhom"
              label="Mã Nhóm"
              rules={[
                {
                  required: true,
                  message: "Hãy chọn mã nhóm",
                },
              ]}
            >
              <Select placeholder="select your gender">
                <Option value="GP01">GP01</Option>
                <Option value="GP02">GP02</Option>
                <Option value="GP03">GP03</Option>
              </Select>
            </Form.Item>

            <Form.Item label="Ngày tạo">
              <DatePicker onChange={handleDatePickerChange} />
            </Form.Item>

            <Form.Item
              label="Tài khoản Người tạo"
              name="taiKhoanNguoiTao"
              initialValue={`${taiKhoanNguoiTao}`}
              rules={[
                {
                  required: true,
                  message: "Hãy nhập mã khóa học!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button className="bg-[#1677ff]" type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </>
      <Input
        onChange={handleOnchange}
        className="py-3 my-5"
        placeholder="Tìm kiếm Khóa học"
      />
      <div>
        <Table columns={columns} dataSource={listCourse} />
      </div>
    </>
  );
}
