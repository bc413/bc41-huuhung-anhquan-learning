import React from "react";
import { useSelector } from "react-redux";
import { GridLoader } from "react-spinners";

export default function Spiner() {
  let { isLoading } = useSelector((state) => {
    return state.spinerSlice;
  });

  return isLoading ? (
    <div
      style={{ backgroundColor: "#d4a373" }}
      className=" fixed top-0 left-0 w-screen h-screen  flex justify-center z-50 items-center "
    >
      <GridLoader color="#fefae0" />
    </div>
  ) : (
    <></>
  );
}
