import axios from "axios";
import { configHeaders, URL_BASE, https } from "./config";

export const getData = {
  getUseData: () => {
    return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01");
  },
  getCourseData: () => {
    return https.get("/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01");
  },
  loginAdmin: (values) => {
    return axios({
      url: `${URL_BASE}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: values,
      headers: configHeaders(),
    });
  },
  deleteUser: (taiKhoan) => {
    return https.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  },
  deleteCourse: (makhoaHoc) => {
    return https.delete(`/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${makhoaHoc}`);
  },
  getSearchUser: (taikhoan) => {
    return https.get(
      `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01&tuKhoa=${taikhoan}`
    );
  },
  getSearchCourse: (tenKhoaHoc) => {
    return https.get(
      `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${tenKhoaHoc}&MaNhom=GP01`
    );
  },

  userRegister: (values) => {
    return axios({
      url: `${URL_BASE}/api/QuanLyNguoiDung/ThemNguoiDung`,
      method: "POST",
      data: values,
      headers: configHeaders(),
    });
  },
  updateUser: (values) => {
    return axios({
      url: `${URL_BASE}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      method: "PUT",
      data: values,
      headers: configHeaders(),
    });
  },
  layDanhMucKhoaHoc: () => {
    return https.get(`/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`);
  },
  themKhoaHoc: (datas) => {
    return axios({
      url: `${URL_BASE}/api/QuanLyKhoaHoc/ThemKhoaHoc`,
      method: "POST",
      data: datas,
      headers: configHeaders(),
    });
  },
  upLoadHinhAnh: (img) => {
    return axios({
      url: `${URL_BASE}/api/QuanLyKhoaHoc/UploadHinhAnhKhoaHoc`,
      method: "POST",
      data: img,
      headers: configHeaders(),
    });
  },
};
