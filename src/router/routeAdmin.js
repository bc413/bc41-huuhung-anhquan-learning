import AdminUsePage from "../Page/AdminUsePage/AdminUsePage";
import AdminMoviePage from "../Page/AdminCoursePage/AdminCoursePage";
import LayoutAdmin from "../Layout/LayoutAdmin";
import LoginPage from "../Page/LoginPage/LoginPage";

export let routeAdmin = [
  {
    url: "/user_admin",
    component: <LayoutAdmin Component={AdminUsePage} />,
  },
  {
    url: "/",
    component: <LayoutAdmin Component={AdminUsePage} />,
  },
  {
    url: "/user_course",
    component: <LayoutAdmin Component={AdminMoviePage} />,
  },
  {
    url: "/login",
    component: <LoginPage />,
  },
];
