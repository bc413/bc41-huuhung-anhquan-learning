import React from "react";
import { useSelector } from "react-redux";
import { localUserServ } from "../service/locailStoage/locailAdmin";
import { Button } from "antd";
import { NavLink } from "react-router-dom";

export default function HeaderComponent() {
  let userInfor = useSelector((state) => {
    return state.useSlice.userInfor;
  });

  let removeLocalStoage = () => {
    localUserServ.remove();
    window.location.reload();
  };

  let renderInfor = () => {
    if (userInfor) {
      return (
        <div className="flex items-center">
          <h2>{userInfor.hoTen}</h2>
          <Button onClick={removeLocalStoage} className="ml-3 text-white">
            Đăng Xuất
          </Button>
        </div>
      );
    } else {
      return (
        <div className="flex items-center ">
          <NavLink to={"/login"}>
            <Button className="ml-3 text-white">Đăng Nhập</Button>
          </NavLink>
        </div>
      );
    }
  };

  return (
    <div className="flex justify-between bg-[#001529] text-white px-[30px]">
      <div></div>
      {renderInfor()}
    </div>
  );
}
