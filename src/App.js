import logo from "./logo.svg";
import "./App.css";
import LayoutAdmin from "./Layout/LayoutAdmin";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { routeAdmin } from "./router/routeAdmin";
import Spiner from "./components/Spiner";

function App() {
  return (
    <div>
      <Spiner />
      <BrowserRouter>
        <Routes>
          {routeAdmin.map((item, index) => {
            return (
              <Route key={index} path={item.url} element={item.component} />
            );
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
