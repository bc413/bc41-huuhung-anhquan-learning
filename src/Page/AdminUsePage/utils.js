import { Tag } from "antd";

export const columns = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (loai) => {
      if (loai == "HV") {
        return (
          <Tag className="bg-[#DEF7EC] text-[#03543F] font-bold border-none">
            Học Viên
          </Tag>
        );
      } else {
        return (
          <Tag className="bg-[#EDEBFE] text-[#5521B5] font-bold border-none">
            Giảng Viên
          </Tag>
        );
      }
    },
  },
  {
    title: "Action",
    dataIndex: "button",
    key: "button",
  },
];

/**
 * {
    "taiKhoan": "123",
    "hoTen": "Quang Beo ",
    "email": "jjjjjjjj@gmail.com",
    "soDt": "12312312312",
    "maLoaiNguoiDung": "GV",
    "button": {
        "type": "div",
        "key": null,
        "ref": null,
        "props": {
            "className": "flex",
            "children": [
                {
                    "type": {
                        "__ANT_BUTTON": true
                    },
                    "key": null,
                    "ref": null,
                    "props": {
                        "className": "mx-3 bg-red-600 text-white",
                        "children": "Delete"
                    },
                    "_owner": null,
                    "_store": {}
                },
                {
                    "type": {
                        "__ANT_BUTTON": true
                    },
                    "key": null,
                    "ref": null,
                    "props": {
                        "className": "bg-amber-500 te text-white",
                        "children": "Edit"
                    },
                    "_owner": null,
                    "_store": {}
                }
            ]
        },
        "_owner": null,
        "_store": {}
    }
}
 */
