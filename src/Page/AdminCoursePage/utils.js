export const columns = [
  {
    title: "Mã khóa học",
    dataIndex: "maKhoaHoc",
    key: "maKhoaHoc",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Tên khóa học",
    dataIndex: "tenKhoaHoc",
    key: "tenKhoaHoc",
  },
  {
    title: "Mô tả",
    dataIndex: "moTa",
    key: "moTa",
    render: (moTa) => {
      return <p className="w-96">{moTa}</p>;
    },
  },
  {
    title: "Hình ảnh",
    dataIndex: "hinhAnh",
    key: "hinhAnh",
    render: (hinhAnh) => (
      <img style={{ width: "200px" }} src={hinhAnh} alt="" />
    ),
  },
  {
    title: "Action",
    dataIndex: "button",
    key: "button",
  },
];
