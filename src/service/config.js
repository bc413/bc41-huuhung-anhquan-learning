import axios from "axios";
import { localUserServ } from "./locailStoage/locailAdmin";

export const URL_BASE = "https://elearningnew.cybersoft.edu.vn";

const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q";

export let configHeaders = () => {
  return {
    TokenCybersoft: token,
    ["Authorization"]: "bearer " + localUserServ.get()?.accessToken,
  };
};

export const https = axios.create({
  baseURL: URL_BASE,
  headers: configHeaders(),
});
